#!/usr/bin/env python3
"""
Lesson 10: Script counting recursively in new processes.
"""
import os
import sys

from argparse import ArgumentParser, Namespace


def get_args() -> Namespace:
    """
    Parse command line arguments.

    :return: Parsed arguments.
    :rtype: Namespace
    """
    parser = ArgumentParser()
    parser.add_argument('value', type=int, help='Recursion level')
    return parser.parse_args()


def main(value: int):
    """
    Main method (entry point).

    :param value: Program argument.
    :type value: int
    """
    print(f'Process pid: {os.getpid()}')
    print(f'Value: {value}')
    if value > 0:
        if os.fork() == 0:
            os.execl(sys.argv[0], 'recursive_call.py', str(value - 1))
        else:
            os.wait()
    if value == 0:
        cmd_input = input("Enter something: ")
        print(f'You enter: {cmd_input}')


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    args = get_args()
    main(args.value)
