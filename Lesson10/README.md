# Lesson 10: Script counting recursively in new processes (works on UNIX system)

**Usage:** 

```
# Under UNIX system
# First of all shall be executable.
# You can add permissions to execute script file as 'chmod u+x recursive_call.py'

./recursive_call.py

or

python3 recursive_call.py
```