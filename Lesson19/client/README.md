# The client program

The client program, takes identifier. Then tries to connect to the server and start 
sending communicates to the server under the UDP protocol.

**Usage:** 

```
# To create new image, try to excute the foolowing command.
# IMPORTANT: Execute command from that folder.
docker build -t clientudp .

# To create container and run new image use the following command.
docker run -it --name client --rm --network pwirnet clientudp 1

# To kill existing container, please execute next command.
docker kill client
```