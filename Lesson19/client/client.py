"""
Lesson 19: The client program, that tries to enter in the critical section.
"""
from argparse import Namespace, ArgumentParser
from random import random
from socket import AF_INET, SOCK_DGRAM, socket
from time import sleep
from typing import NoReturn


def get_args() -> Namespace:
    """
    Add argument parser to script and return namespace with parsed command line arguments.

    :return: Namespace with parsed command line arguments.
    :rtype: Namespace
    """
    parser = ArgumentParser()
    parser.add_argument('id', type=int, help='Client identifier.')
    return parser.parse_args()


class Mutex:
    """
    Class simulates critical section, share operations such as: acquire, release, close.
    """

    def __init__(self, addr):
        self.addr = addr
        self.server_socket = socket(AF_INET, SOCK_DGRAM)

    def acquire(self) -> NoReturn:
        """
        Send request to server and try to acquire lock on the critical section.
        """
        self.server_socket.sendto(b'Request', self.addr)
        data, address = self.server_socket.recvfrom(512)
        print(f'Received: "{data.decode("utf-8")}", enter into critical section.')

    def release(self) -> NoReturn:
        """
        Release critical section and send information to the server that critical section is free now.
        """
        print('Exit from critical section, send "Release".')
        self.server_socket.sendto(b'Release', self.addr)

    def close(self):
        """
        Close the socket that used to communicates with a server.
        """
        self.server_socket.close()


def main(identifier: int) -> NoReturn:
    """
    Main method (entry point). Connects to the server, then send message to the server and wait on the response.

    :param identifier: Client identifier.
    :type identifier: int
    """
    lock = Mutex(('server', 3000))

    for index in range(5):
        sleep(random())
        lock.acquire()
        for inner_index in range(10):
            print(f'Node {identifier}, index={index}, inner_index={inner_index}')
            sleep(random())
        lock.release()

    lock.close()


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments should be taken from command line and then
    executes main method.
    """
    args = get_args()
    main(args.id)
