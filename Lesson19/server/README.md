# The server program

The server program handles communicates that it receives, and executes different actions based on communicate message.

**Usage:** 

```
# To create new image, try to excute the foolowing command.
# IMPORTANT: Execute command from that folder.
docker build -t serverudp .

# To create container and run new image use the following command.
docker run -it --name server --rm --network pwirnet serverudp

# To kill existing container, please execute next command.
docker kill server
```