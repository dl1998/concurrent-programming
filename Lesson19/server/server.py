"""
Lesson 19: The server program, that handles communicates from clients.
"""
from queue import Queue
from socket import AF_INET, SOCK_DGRAM, socket
from typing import NoReturn


def main() -> NoReturn:
    """
    Main method (entry point).
    Run the server.
    """
    critical_section_is_free = True
    queue = Queue()
    with socket(AF_INET, SOCK_DGRAM) as listener_socket:
        listener_socket.bind(('', 3000))
        print('Waiting for messages')
        while True:
            data, address = listener_socket.recvfrom(2048)
            print(f'Received communicate: "{data.decode("utf-8")}", from {address}')
            if data == b'Request' and critical_section_is_free:
                listener_socket.sendto(b'Reply', address)
                critical_section_is_free = False
            elif data == b'Request' and not critical_section_is_free:
                queue.put(address)
            elif data == b'Release':
                critical_section_is_free = True
                if queue.empty():
                    print('Queue is empty.')
                else:
                    address = queue.get()
                    listener_socket.sendto(b'Reply', address)
                    critical_section_is_free = False


if __name__ == '__main__':
    """
    If that module will be executed directly then executes main method.
    """
    main()
