"""
Lesson 14: Program calculate sum of digit squares from 1 to 50.
"""
from multiprocessing import Process, Queue
from typing import NoReturn


class Mapper(Process):
    """
    Class responsible for digit square calculation, after calculation result sends to Adder process.
    """

    __queue_one: Queue
    __queue_two: Queue

    def __init__(self, queue_one: Queue, queue_two: Queue) -> NoReturn:
        Process.__init__(self)
        self.__queue_one = queue_one
        self.__queue_two = queue_two

    def run(self) -> NoReturn:
        """
        Run mapper process, get Tuple[bool, float] from queue, calculate square for float and send it to Added process.
        Do that until bool value is True.
        """
        while True:
            pair = self.__queue_one.get()
            self.__queue_two.put((True, pair[1] ** 2))
            if pair[0] is False:
                break


class Adder(Process):
    """
    Class responsible for digits summing.
    """
    __queue_two: Queue
    __sum: float

    def __init__(self, queue_two: Queue) :
        Process.__init__(self)
        self.__queue_two = queue_two
        self.__sum = 0.0

    def run(self) -> NoReturn:
        """
        Initialize sum variable as 0, get Tuple[bool, float] from queue, add float value to sum variable.
        Do that until bool value is True.
        """
        while True:
            pair = self.__queue_two.get()
            self.__sum += pair[1]
            if pair[0] is False:
                break
        print(self.__sum)


def main() -> NoReturn:
    """
    Main method (entry point).
    """
    queue_one = Queue()
    queue_two = Queue()
    mappers = [Mapper(queue_one, queue_two) for _ in range(5)]
    adder = Adder(queue_two)
    for mapper in mappers:
        mapper.start()
    adder.start()
    for x in range(1, 51, 1):
        queue_one.put((True, float(x)))
    for x in range(5):
        queue_one.put((False, 0))
    for mapper in mappers:
        mapper.join()
    queue_two.put((False, 0))
    adder.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()
