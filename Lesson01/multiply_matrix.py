"""
Lesson 1: program multiply two matrix.
"""
from argparse import ArgumentParser, Namespace
import threading

from typing import Tuple, NoReturn


class MultiplyThread(threading.Thread):
    """
    MultiplyThread class, responsible for multiplication of one specific row and column.
    """
    __thread_number: int
    __matrix_a: list
    __matrix_b: list
    __row_matrix_a: int
    __column_matrix_b: int
    __multiplied_matrix: list

    def __init__(self, thread_number: int, matrix_a: list, matrix_b: list, row_matrix_a: int, column_matrix_b: int,
                 multiplied_matrix: list) -> NoReturn:
        """
        Init method for class MultiplyThread.

        :param thread_number: Define thread unique number.
        :type thread_number: int
        :param matrix_a: First matrix.
        :type matrix_a: list
        :param matrix_b: Second matrix.
        :type matrix_b: list
        :param row_matrix_a: Row from first matrix which has been multiplied.
        :type row_matrix_a: int
        :param column_matrix_b: Column from second matrix which has been multiplied.
        :type column_matrix_b: int
        :param multiplied_matrix: Matrix wich store multiplication result.
        :type multiplied_matrix: list
        """
        threading.Thread.__init__(self)
        self.__thread_number = thread_number
        self.__matrix_a = matrix_a
        self.__matrix_b = matrix_b
        self.__row_matrix_a = row_matrix_a
        self.__column_matrix_b = column_matrix_b
        self.__multiplied_matrix = multiplied_matrix

    @staticmethod
    def __get_row(matrix: list, searched_row_index: int) -> list:
        """
        Method return specific row from matrix.

        :param matrix: Matrix from which row will be returned.
        :type matrix: list
        :param searched_row_index: Index of the row which shall be returned.
        :type searched_row_index: int
        :return: Specific row from matrix.
        :rtype: list
        """
        return matrix[searched_row_index]

    @staticmethod
    def __get_column(matrix: list, searched_column_index: int) -> list:
        """
        Method return specific column from matrix.

        :param: matrix: Matrix from which column will be returned.
        :type matrix: list
        :param searched_column_index: Index of the column which shall be returned.
        :type searched_column_index: int
        :return: Specific column from matrix.
        :rtype: list
        """
        column = []
        for row in matrix:
            for column_index, column_value in enumerate(row):
                if column_index == searched_column_index:
                    column.append(column_value)
        return column

    @staticmethod
    def __multiply_row_and_column(row: list, column: list) -> float:
        """
        Multiply row and column.

        :param row: Row to multiply.
        :type row: list
        :param column: Column to multiply.
        :type column: list
        :return: Sum of row and column multiplication.
        :rtype: float
        """
        return sum(row[i] * column[i] for i in range(len(row)))

    def run(self) -> NoReturn:
        """
        Method run thread, which multiply row and column.
        """
        row = self.__get_row(self.__matrix_a, self.__row_matrix_a)
        column = self.__get_column(self.__matrix_b, self.__column_matrix_b)
        self.__multiplied_matrix[self.__row_matrix_a][self.__column_matrix_b] = self.__multiply_row_and_column(row,
                                                                                                               column)
        print(f'Thread number: {self.__thread_number}. Multiply row: {self.__row_matrix_a} and '
              f'column: {self.__column_matrix_b}.')


def get_args() -> Namespace:
    """
    Add argument parser to script and return namespace with parsed command line arguments.

    :return: Namespace with parsed command line arguments.
    :rtype: Namespace
    """
    parser = ArgumentParser()
    parser.add_argument('--input-file', type=str, required=True, help='Input file with two matrices')
    return parser.parse_args()


def read_matrix(input_file: str) -> Tuple[list, list]:
    """
    Read two matrix from file and return them.

    :param input_file: File with input, contains two matrices.
    :type input_file: str
    :return: Two matrices from input file.
    :rtype: Tuple[list, list]
    """
    with open(file=input_file, mode='r', encoding='UTF8') as file:
        lines = file.readlines()
        matrix_a = []
        matrix_b = []
        filled_matrix = matrix_a
        for line in lines:
            line = line.rstrip()
            if not line:
                filled_matrix = matrix_b
                continue
            filled_matrix.append([float(x) for x in line.split()])
        return matrix_a, matrix_b


def multiply_matrix(matrix_a, matrix_b):
    """
    Method multiply two matrices and return new matrix.

    :param matrix_a: First matrix to multiply.
    :type matrix_a: list
    :param matrix_b: Second matrix to multiply.
    :type matrix_b: list
    :return: New matrix, result of multiplication of two matrices.
    :rtype: list
    """
    m, n = len(matrix_a), len(matrix_b[0])
    multiplied_matrix = [[0 for _ in range(n)] for _ in range(m)]
    threads = []
    thread_number = 1
    for i in range(m):
        for j in range(n):
            threads.append(MultiplyThread(thread_number, matrix_a, matrix_b, i, j, multiplied_matrix))
            thread_number += 1

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    return multiplied_matrix


def print_matrix(matrix: list) -> NoReturn:
    """
    Print matrix by rows and columns.

    :param matrix: Matrix to print.
    :type matrix: list
    """
    for row in matrix:
        for column in row:
            print(f'{column:5.2} ', end='')
        print()


def main(input_file: str) -> NoReturn:
    """
    Main method (entry point).

    :param input_file: File with two matrices.
    :type input_file: str
    """
    matrix_a, matrix_b = read_matrix(input_file)
    multiplied_matrix = multiply_matrix(matrix_a, matrix_b)
    print_matrix(multiplied_matrix)


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments shall be taken from command line, and then
    execute main method.
    """
    args = get_args()
    main(args.input_file)
