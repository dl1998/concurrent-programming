# Lesson 2: producer - consumer problem

**Usage:** 

```
# Using defualt buffer size. Default buffer size is 2.
python producer_consumer.py

# With specific buffer size.
python producer_consumer.py --buffer-size 10
```    