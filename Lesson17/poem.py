"""
Lesson 17: Program that prints poem in multiple threads.
"""
from threading import Thread, RLock
from time import sleep
from random import random, seed
from argparse import ArgumentParser, Namespace
from typing import List, NoReturn


def get_args() -> Namespace:
    """
    Add argument parser to script and return namespace with parsed command line arguments.

    :return: Namespace with parsed command line arguments.
    :rtype: Namespace
    """
    parser = ArgumentParser()
    parser.add_argument('--input-file', type=str, required=True, help='Input file with a poem.')
    return parser.parse_args()


class MyThread(Thread):
    """
    Class responsible for printing poem in separate threads. Takes one line that shall be printed in separate thread.
    """
    __print_lock: RLock

    def __init__(self, line: str, target: List[str], lock: RLock) -> NoReturn:
        Thread.__init__(self)
        self.line = line
        self.target = target
        self.__print_lock = lock
        self.__lock = RLock()

    def run(self) -> NoReturn:
        """
        Start printing poem character by character.
        """
        with self.__lock:
            for index, character in enumerate(self.line):
                sleep(random())
                self.target[index] = character
                with self.__print_lock:
                    print(f'{index} {character}')


def read_lines(file_path: str) -> List[str]:
    """
    Read lines from file, remove new line character.

    :param file_path: Path to file.
    :type file_path: str
    :return: List of read lines.
    :rtype: List[str]
    """
    with open(file_path, 'r') as file:
        lines = [line.replace('\r\n', '').replace('\n', '') for line in file.readlines()]
    return lines or []


def main(poem_file: str) -> NoReturn:
    """
    Main method (entry point).

    :param poem_file: The path to the file with a poem.
    :type poem_file: str
    """
    seed()
    lines = read_lines(poem_file)
    target = [' '] * max(len(line) for line in lines)
    lock = RLock()
    threads = [MyThread(line, target, lock) for line in lines]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    print(''.join(target))


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments should be taken from command line and then
    executes main method.
    """
    args = get_args()
    main(args.input_file)
