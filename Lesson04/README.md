# Lesson 4: N threads barrier (implemented on semaphores)

**Usage:**

```
# Execute with default elements count (default count is 10)
python barrier.py

# Execute with custom elements count
python barrier.py --elements 15
```