"""
Lesson 4: Program illustrate n threads barrier (implemented on semaphores).
"""

import argparse
import threading

from time import sleep
from random import random
from typing import NoReturn


def get_args() -> argparse.Namespace:
    """
    Return namespace with parsed command line arguments.

    :return: Command line arguments.
    :rtype: argparse.Namespace
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--elements', type=int, required=False, default=10, help='Elements count.')
    return parser.parse_args()


class Counter(object):
    """
    Class implement counter for bridge.
    """
    __lock: threading.RLock
    __semaphore: threading.Semaphore
    __elements: int

    def __init__(self, semaphore: threading.Semaphore, elements: int):
        """
        Init method for Counter class.

        :param semaphore: Semaphore used to control bridge lock.
        :type semaphore: threading.Semaphore
        :param elements: Elements count.
        :type elements: int
        """
        self.__lock = threading.RLock()
        self.__semaphore = semaphore
        self.__elements = elements

        self.__semaphore.acquire()

    def __enter__(self) -> NoReturn:
        """
        Context manager, enter to critical section.
        """
        self.__semaphore.acquire()

    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn:
        self.__semaphore.release()

    def count(self) -> NoReturn:
        """
        Method decrease counter variable until it will not be a zero. After it reaches zero lock will be released.
        """
        with self.__lock:
            self.__elements -= 1
            if self.__elements == 0:
                self.__semaphore.release()


class MyThread(threading.Thread):
    """
    Class represent thread which execute something and wait until bridge will not be open.
    """
    __thread_number: int
    __counter: Counter

    def __init__(self, thread_number: int, counter: Counter) -> NoReturn:
        """
        Init method for MyThread class.

        :param thread_number: Thread unique identifier.
        :type thread_number: int
        :param counter: Counter instance used to manipulate counter.
        :type counter: Counter
        """
        threading.Thread.__init__(self)
        self.__thread_number = thread_number
        self.__counter = counter

    def run(self) -> NoReturn:
        """
        Start simulation for thread.
        """
        sleep(random())
        print(f'Thread {self.__thread_number} executed A')
        sleep(random())
        self.__counter.count()
        with self.__counter:
            print(f'Thread {self.__thread_number} executed B')
        sleep(random())


def main(elements: int) -> NoReturn:
    """
    Main method (entry point).

    :param elements: Simulation elements count.
    :type elements: int
    """
    semaphore = threading.Semaphore()
    counter = Counter(semaphore, elements)
    threads = [MyThread(index, counter) for index in range(elements)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments shall be taken from command line, and then
    execute main method.
    """
    args = get_args()
    main(args.elements)
