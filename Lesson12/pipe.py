#!/usr/bin/env python3
"""
Lesson 12: Python script which simulate ps aux | grep '^root' action.
"""
from typing import NoReturn
from os import pipe, fork, wait, execl, dup2, close


def main() -> NoReturn:
    """
    Main method (entry point).
    """
    read, write = pipe()
    # Create child process, then execute grep in parent and ps in child process.
    if fork() > 0:
        close(write)
        dup2(read, 0)
        execl('/usr/bin/grep', 'grep', '^root')
    else:
        close(read)
        dup2(write, 1)
        execl('/usr/bin/ps', 'ps', 'aux')


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()
