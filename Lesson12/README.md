# Lesson 12: Python script which simulate ps aux | grep '^root' action (works on UNIX system)

**Usage:** 

```
# Under UNIX system
# First of all shall be executable.
# You can add permissions to execute script file as 'chmod u+x pipe.py'

./pipe.py

or

python3 pipe.py
```