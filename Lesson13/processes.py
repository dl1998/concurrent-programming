"""
Lesson 13: Chain of 10 processes.
"""
import os

from time import sleep
from multiprocessing import Process
from typing import NoReturn


class ExampleProcess(Process):
    """
    Example process class, create chain of 10 processes.
    """

    __process_number: int

    def __init__(self, process_number: int = 1):
        Process.__init__(self)
        self.__process_number = process_number

    def __info(self) -> NoReturn:
        """
        Utility method, print process details.
        """
        print('*' * 20)
        print(f'Process in chain: {self.__process_number}')
        print(f'PID: {os.getpid()}')
        print(f'PPID: {os.getppid()}')
        print('*' * 20)

    def run(self) -> NoReturn:
        """
        Run program, start processes chain creation.
        """
        self.__info()
        if self.__process_number < 10:
            new_process = ExampleProcess(self.__process_number + 1)
            new_process.start()
            new_process.join()
        else:
            sleep(3)


def main() -> NoReturn:
    """
    Main method (entry point).
    """
    process = ExampleProcess()
    process.start()
    process.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()

