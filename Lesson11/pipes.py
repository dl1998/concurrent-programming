#!/usr/bin/env python3
"""
Lesson 11: Pipes based process communication.
"""
import os
from random import randint, uniform


def main():
    """
    Main method (entry point).
    """
    read_parent, write_parent = os.pipe()
    read_child, write_child = os.pipe()
    if os.fork() > 0:
        random_digit = randint(0, 200)
        print(f'Iterations count: {random_digit}')
        # Close unused by parent process, files descriptors
        os.close(read_parent)
        os.close(write_child)
        with os.fdopen(write_parent, 'w') as write_stream:
            with os.fdopen(read_child, 'r') as read_stream:
                for _ in range(random_digit):
                    send_digit = uniform(0, 10)
                    print(str(send_digit), file=write_stream)
                    write_stream.flush()
                    print(f'Send: {send_digit:3.6f}, returned: {float(read_stream.readline().strip()):3.6f}')
        os.wait()
    else:
        # Close unused by child process, files descriptors
        os.close(read_child)
        os.close(write_parent)
        with os.fdopen(write_child, 'w') as write_stream:
            with os.fdopen(read_parent, 'r') as read_stream:
                for line in read_stream:
                    digit = float(line)
                    digit = digit ** 2
                    print(str(digit), file=write_stream)
                    write_stream.flush()


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()
