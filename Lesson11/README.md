# Lesson 11: Pipes based process communication (works on UNIX system)

**Usage:** 

```
# Under UNIX system
# First of all shall be executable.
# You can add permissions to execute script file as 'chmod u+x pipes.py'

./pipes.py

or

python3 pipes.py
```