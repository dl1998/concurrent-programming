#!/usr/bin/env python3
"""
Lesson 9: Script which create chain from 11 processes.
"""
import os
from time import sleep


def main():
    """
    Main method (entry point).
    """
    print(f'Parent pid: {os.getpid()}')
    for iteration in range(10):
        new_process = os.fork() == 0
        if new_process:
            print(f'Created new child process with pid: {os.getpid()}.')
        else:
            os.wait()
            break
        if new_process and iteration == 9:
            print('Enter in the infinite loop.')
            while True:
                sleep(1)


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()
