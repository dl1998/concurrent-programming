"""
Lesson 3: Program illustrate reader - writer problem.
"""

import threading
import time

from random import random
from typing import NoReturn, Union

reader_writer: 'ReaderWriterLock'


class ReaderWriterLock:
    """
    Class realizes lock for writers and readers.
    """
    __condition_variable: threading.Condition
    __readers: int
    __waiting_writers: int
    __writer_active: bool

    def __init__(self) -> NoReturn:
        """
        Init method for lock class.
        """
        self.__condition_variable = threading.Condition()
        self.__readers = 0
        self.__waiting_writers = 0
        self.__writer_active = False

    def __enter__(self):
        """
        Context manager, allocate resource on the beginning.
        """
        self.__condition_variable.acquire()

    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn:
        """
        Context manager, release resource in the end.

        :param exc_type: Exception value.
        :param exc_val: Exception type.
        :param exc_tb: Exception traceback.
        """
        self.__condition_variable.release()

    def __reader_can_read(self) -> bool:
        """
        Check can reader read or not.

        :return: If reader can read then True, else False.
        :rtype: bool
        """
        return not self.__writer_active and self.__waiting_writers == 0

    def __writer_can_write(self) -> bool:
        """
        Check can writer write or not.

        :return: If writer can write then True, else False.
        :rtype: bool
        """
        return not self.__writer_active and self.__readers == 0

    def reader_lock(self) -> NoReturn:
        """
        Method try to lock resource for reader in the loop, so long as condition not met. It check condition, if
        condition not met then wait until another thread notify it again. After next notification check condition again,
        that action will be repeated as long as condition will not be met. When condition will be met loop will be break
        and count of reader will be increased by 1.
        """
        with self.__condition_variable:
            while not self.__reader_can_read():
                self.__condition_variable.wait()
            self.__readers += 1

    def reader_unlock(self) -> NoReturn:
        """
        Decrement number of readers and notify all threads if last reader ends reading.
        """
        with self.__condition_variable:
            self.__readers -= 1
            if self.__readers == 0:
                self.__condition_variable.notify_all()

    def writer_lock(self) -> NoReturn:
        """
        Method try to lock resource for writer in the loop, so long as condition not met. It check condition, if
        condition not met then wait until another thread notify it again. After next notification check condition again,
        that action will be repeated as long as condition will not be met. When condition will be met then break the
        loop and decreased count of waiting writers by 1 also set writer_active flag on True.
        """
        with self.__condition_variable:
            self.__waiting_writers += 1
            while not self.__writer_can_write():
                self.__condition_variable.wait()
            self.__waiting_writers -= 1
            self.__writer_active = True

    def writer_unlock(self) -> NoReturn:
        """
        Change writer to inactive and notify all threads if last writer ends writing.
        """
        with self.__condition_variable:
            self.__writer_active = False
            self.__condition_variable.notify_all()


class Reader(threading.Thread):
    """
    Class represent a reader.
    """
    __reader_identifier: int

    def __init__(self, reader_identifier: int) -> NoReturn:
        """
        Init method for reader, take reader identifier.

        :param reader_identifier: Unique identifier for new reader.
        :type reader_identifier: int
        """
        threading.Thread.__init__(self)
        self.__reader_identifier = reader_identifier

    def run(self) -> NoReturn:
        """
        Start competition for resource by reader.
        """
        for index in range(5):
            time.sleep(random())
            reader_writer.reader_lock()
            with reader_writer:
                print(f'Reader {self.__reader_identifier} starts reading, {index}')
            time.sleep(random())
            with reader_writer:
                print(f'Reader {self.__reader_identifier} stops reading, {index}')
            reader_writer.reader_unlock()


class Writer(threading.Thread):
    """
    Class represent a writer.
    """
    __writer_identifier: int

    def __init__(self, writer_identifier: int) -> NoReturn:
        """
        Init method for writer, take writer identifier.

        :param writer_identifier: Unique writer identifier.
        :type writer_identifier: int
        """
        threading.Thread.__init__(self)
        self.__writer_identifier = writer_identifier

    def run(self) -> NoReturn:
        """
        Start competition for resource by writer.
        """
        for index in range(5):
            time.sleep(random())
            reader_writer.writer_lock()
            with reader_writer:
                print(f'Writer {self.__writer_identifier} starts writing, {index}')
            time.sleep(random())
            with reader_writer:
                print(f'Writer {self.__writer_identifier} stops writing, {index}')
            reader_writer.writer_unlock()


def factory(identifier) -> Union[Reader, Writer]:
    """
    Using factory to return randomly new Reader or Writer.

    :param identifier: Unique identifier for new Reader or Writer.
    :type identifier: int
    :return: New Reader or Writer
    :rtype: Union[Reader, Writer]
    """
    if random() < 0.5:
        return Reader(identifier)
    else:
        return Writer(identifier)


def main() -> NoReturn:
    """
    Main method (entry point).
    """
    global reader_writer
    reader_writer = ReaderWriterLock()

    threads = [factory(index) for index in range(10)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()
