#!/usr/bin/env python3
"""
Lesson 8: Script which create 10 child processes and wait on them.
"""
import os
from time import sleep


def main():
    """
    Main method (entry point).
    """
    print(f'Parent pid: {os.getpid()}')
    for _ in range(10):
        if os.fork() == 0:
            print(f'Child with pid: {os.getpid()}, enter in infinite loop.')
            while True:
                sleep(1)
    os.wait()


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()
