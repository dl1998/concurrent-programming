# Lesson 8: Script which create 10 child processes and wait on them (works on UNIX system)

**Usage:** 

```
# Under UNIX system
# First of all shall be executable.
# You can add permissions to execute script file as 'chmod u+x fork_processes.py'

./fork_processes.py

or

python3 fork_processes.py 
```