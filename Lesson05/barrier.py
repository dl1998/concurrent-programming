"""
Lesson 5: Program illustrate reusable barrier (implemented on semaphores).
"""

import argparse
import threading
from random import random
from time import sleep
from typing import NoReturn


def get_args() -> argparse.Namespace:
    """
    Return namespace with parsed command line arguments.

    :return: Command line arguments.
    :rtype: argparse.Namespace
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--elements', type=int, required=False, default=10, help='Elements count.')
    parser.add_argument('--iterations', type=int, required=False, default=5, help='Iterations count.')
    return parser.parse_args()


class Counter(object):
    """
    Class implement counter for bridge.
    """
    __lock: threading.RLock
    __semaphore_a: threading.Semaphore
    __semaphore_b: threading.Semaphore
    __elements: int
    __elements_count: int
    __is_decreased: bool

    def __init__(self, semaphore_a: threading.Semaphore, semaphore_b: threading.Semaphore, elements: int):
        """
        Init method for Counter class.

        :param semaphore_a: Semaphore used to control bridge a lock.
        :type semaphore_a: threading.Semaphore
        :param semaphore_b: Semaphore used to control bridge b lock.
        :type semaphore_b: threading.Semaphore
        :param elements: Elements counter.
        :type elements: int
        """
        self.__lock = threading.RLock()
        self.__semaphore_a = semaphore_a
        self.__semaphore_b = semaphore_b
        self.__elements_count = elements
        self.__elements = 0

        self.__is_decreased = False
        self.__semaphore_b.acquire()

    @property
    def bridge_a(self) -> threading.Semaphore:
        """
        Semaphore for 'bridge a'

        :return: Semaphore
        :rtype: threading.Semaphore
        """
        return self.__semaphore_a

    @property
    def bridge_b(self) -> threading.Semaphore:
        """
        Semaphore for 'bridge b'

        :return: Semaphore
        :rtype: threading.Semaphore
        """
        return self.__semaphore_b

    def count(self) -> NoReturn:
        """
        Method decrease counter variable until it will not be a zero. After it reaches zero 'bridge a' lock will be
        released and flag will be changed. Next counter starts increase it value, until it will not be equal to elements
        count. When counter reaches element count value 'bridge b' lock will be released and flag will be changed again.
        """
        with self.__lock:
            if self.__is_decreased:
                self.__elements -= 1
                if self.__elements == 0:
                    self.__is_decreased = False
                    self.__semaphore_b.acquire()
                    self.__semaphore_a.release()
            else:
                self.__elements += 1
                if self.__elements == self.__elements_count:
                    self.__is_decreased = True
                    self.__semaphore_a.acquire()
                    self.__semaphore_b.release()


class MyThread(threading.Thread):
    """
    Class represent thread which execute something and wait until bridge will not be open.
    """
    __thread_number: int
    __counter: Counter
    __iterations: int

    def __init__(self, thread_number: int, counter: Counter, iterations: int) -> NoReturn:
        """
        Init method for MyThread class.

        :param thread_number: Thread unique identifier.
        :type thread_number: int
        :param counter: Counter instance used to manipulate counter.
        :type counter: Counter
        :param iterations: Iterations to repeat for thread.
        :type iterations: int
        """
        threading.Thread.__init__(self)
        self.__thread_number = thread_number
        self.__counter = counter
        self.__iterations = iterations

    def run(self) -> NoReturn:
        """
        Start simulation for thread.
        """
        for index in range(self.__iterations):
            sleep(random())
            self.__counter.count()
            with self.__counter.bridge_b:
                print(f'Thread {self.__thread_number} of {index} iteration executed A')
            sleep(random())
            self.__counter.count()
            with self.__counter.bridge_a:
                print(f'Thread {self.__thread_number} of {index} iteration executed B')
            sleep(random())


def main(elements: int, iterations: int) -> NoReturn:
    """
    Main method (entry point).

    :param elements: Simulation elements count.
    :type elements: int
    :param iterations: Count of iterations for thread.
    :type iterations: int
    """
    semaphore_a = threading.Semaphore()
    semaphore_b = threading.Semaphore()
    counter = Counter(semaphore_a, semaphore_b, elements)
    threads = [MyThread(index, counter, iterations) for index in range(elements)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments shall be taken from command line, and then
    execute main method.
    """
    args = get_args()
    main(args.elements, args.iterations)
