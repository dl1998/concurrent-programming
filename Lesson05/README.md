# Lesson 5: Reusable barrier (implemented on semaphores)

**Usage:**

```
# Execute with default elements and iterations count (default elements count is 10, default iterations count is 5)
python barrier.py

# Execute with custom elements count and iterations count
python barrier.py --elements 15 --iterations 10
```