# Concurrent Programming 

Repository for concurrent programming lessons.

## Lesson 1: Multiply two matrices
Program take input file with two matrices and multiply them.

## Lesson 2: Producer - Consumer problem
Program take buffer size and illustrate producer - consumer problem.

## Lesson 3: Reader - Writer problem
Program illustrate reader - writer problem. How readers and writers try to compete for resources.

## Lesson 4: N threads barrier (implemented on semaphore)
Program illustrate barrier for n threads using semaphore. In that realization can be passed elements count.

## Lesson 5: Reusable barrier (implemented on semaphore)
Program illustrate reusable barrier using semaphore. In that realization can be passed elements and iterations count.

## Lesson 6: Producer - Consumer problem (implemented on semaphore)
Program illustrate producer - consumer problem, implemented using semaphore.

## Lesson 7: Philosopher problem (implemented on semaphore)
**The program consists of two parts:**

- Implementation avoiding deadlocks
- Implementation preventing deadlocks

## Lesson 8: Script which create 10 child processes and wait on them (works on UNIX system)
Parent process create 10 new child processes, all child processes enter in the infinite loop.

## Lesson 9: Script which create chain from 11 processes (works on UNIX system)
Parent process create child, all children creates exactly one child until chain of the processes will not counts 11 processes.

## Lesson 10: Script counting recursively in new processes (works on UNIX system)
Script take a value, then if value more then 0 it split into two processes. Parent process wait on the end of program, child process replace itself on the same program called with value - 1 argument.

## Lesson 11: Pipes based process communication (works on UNIX system)
Script send digit to another process and wait until process return digit square back.

## Lesson 12: Python script which simulate ps aux | grep '^root' action (works on UNIX system)
Script create pipe and two new processes. First process perform ps aux action and write result to standard output. Second process perform grep '^root' action on standard output.

## Lesson 13: Chain of 10 processes (using multiprocessing)
Script creates chain of 10 processes, where last process sleep 3 seconds and finish own execution.

## Lesson 14: Program calculate sum of digit squares from 1 to 50 (using multiprocessing)
Script calculate sum of squares for digits from 1 to 50 with step 1.
For calculation used two classes: Mapper and Adder, where:
- Mapper calculate square for digit and send it to Adder
- Adder sum all digits which receive from Mapper
- At the same time perform 10 Mappers and 1 Adder processes

## Lesson 15: Program creates 10 processes and token, then transmit token in chain (using multiprocessing)
Script creates chain of 10 processes and 1 token, then transmit token in chain.

## Lesson 16: Program multiply two matrices (using futures)
Script multiply two matrices, to multiply row by column used feature library.

## Lesson 17: Program that prints poem in multiple threads
Script reads a poem from the file and prints it character by character. Where every line executes in own thread.

## Lesson 18: Client-Server application, uses TCP protocol to demonstrate how to implement messages transmitting
Script illustrate message transmitting between client and server, under the TCP protocol.

## Lesson 19: Client-Server application, uses UDP protocol to demonstrate mutual exclusion
Script illustrate communication between server and client using UDP protocol. Client tries to enter to the critical section and server grant access to the critical section, if it free.