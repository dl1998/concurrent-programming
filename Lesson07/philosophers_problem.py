"""
Lesson 7: Program illustrate philosophers problem (implemented on semaphore).
"""
from argparse import ArgumentParser, Namespace
from enum import Enum
from threading import Thread, Semaphore
from time import sleep
from random import random
from typing import NoReturn, List, Tuple, Optional


def get_args() -> Namespace:
    """
    Add argument parser to script and return namespace with parsed command line arguments.

    :return: Namespace with parsed command line arguments.
    :rtype: Namespace
    """
    parser = ArgumentParser()
    parser.add_argument('--implementation', type=str, required=True,
                        choices=['avoiding-deadlocks', 'preventing-deadlocks'],
                        help='One of the following implementations: avoiding deadlocks or preventing deadlocks.')
    parser.add_argument('--iterations', type=int, default=100, help='Iteration for philosophers.')
    return parser.parse_args()


class Direction(Enum):
    """
    Enum contains two available directions (left and right).
    """
    LEFT = 0
    RIGHT = 1

    def __str__(self):
        return self.name.lower()


class Philosopher(Thread):
    """
    Class represent philosopher which will compete for forks.
    """
    __waiter: Semaphore
    __forks: List[Semaphore]
    __identifier: int
    __iterations: int

    def __init__(self, forks: List[Semaphore], identifier: int, iterations: int,
                 waiter: Optional[Semaphore] = None) -> NoReturn:
        """
        Init method for Philosopher class.

        :param forks: List of semaphores which represent forks.
        :type forks: List[Semaphore]
        :param identifier: Identifier of the philosopher.
        :type identifier: int
        :param iterations: How much times philosopher will be eat.
        :type iterations: int
        :param waiter: Semaphore which represent critical section.
        :type waiter: Optional[Semaphore]
        """
        Thread.__init__(self)
        self.__waiter = waiter
        self.__forks = forks
        self.__identifier = identifier
        self.__iterations = iterations

    def fork(self, direction: Direction) -> Semaphore:
        """
        Return left or right fork.

        :param direction: Fork which will be taken (left or right).
        :type direction: Direction
        :return: Semaphore which represent a fork.
        :rtype: Semaphore
        """
        if direction == Direction.LEFT:
            return self.__forks[self.__identifier]
        else:
            return self.__forks[(self.__identifier + 1) % 5]

    def fork_order(self) -> Tuple[Direction, Direction, Direction, Direction]:
        """
        The method select the order in which forks will be taken and laid back.

        :return: Four directions, represents: take first, take second, put first, put second.
        :rtype: Tuple[Direction, Direction, Direction, Direction]
        """
        if self.__identifier != 4:
            take_first, take_second = Direction.LEFT, Direction.RIGHT
        else:
            take_first, take_second = Direction.RIGHT, Direction.LEFT
        if random() < 0.5:
            put_first, put_second = Direction.LEFT, Direction.RIGHT
        else:
            put_first, put_second = Direction.RIGHT, Direction.LEFT
        return take_first, take_second, put_first, put_second

    def run(self) -> NoReturn:
        """
        Run simulation for the philosopher.
        """
        for iteration in range(self.__iterations):
            if self.__waiter:
                self.__waiter.acquire()
            print(f'Philosopher {self.__identifier} thinks {iteration}th iteration')
            sleep(random())
            take_first, take_second, put_first, put_second = self.fork_order()
            print(f'Philosopher {self.__identifier} takes {take_first} fork {iteration}th iteration')
            self.fork(take_first).acquire()
            sleep(random())
            print(f'Philosopher {self.__identifier} takes {take_second} fork {iteration}th iteration')
            self.fork(take_second).acquire()
            print(f'Philosopher {self.__identifier} eats {iteration} th iteration')
            sleep(random())
            print(f'Philosopher {self.__identifier} puts down {put_first} fork {iteration}th iteration')
            self.fork(put_first).release()
            sleep(random())
            print(f'Philosopher {self.__identifier} puts down {put_second} fork {iteration}th iteration')
            self.fork(put_second).release()
            if self.__waiter:
                self.__waiter.release()


def main(implementation: str, iterations: int) -> NoReturn:
    """
    Main method (entry point).

    :param implementation: Implementation of the philosophers problem, can be one of two: avoiding deadlocks or
    preventing deadlocks.
    :type implementation: str
    :param iterations: How much times each philosopher will be eat.
    :type iterations: int
    """
    waiter = None
    if implementation == 'preventing-deadlocks':
        waiter = Semaphore(4)
    forks = [Semaphore() for _ in range(5)]
    philosophers = [Philosopher(forks, ordinal, iterations, waiter) for ordinal in range(5)]

    for philosopher in philosophers:
        philosopher.start()

    for philosopher in philosophers:
        philosopher.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments shall be taken from command line, and then
    execute main method.
    """
    args = get_args()
    print(f'{args.implementation}')
    main(args.implementation, args.iterations)
