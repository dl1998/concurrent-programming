# Lesson 7: philosophers problem (implemented on semaphore)

**Usage:** 

```
# To choose implementation shall be run with flag --implementation, flag is mandatory.
# Available options for --implementation flag are 'avoiding-deadlocks' and 'preventing-deadlocks'.
# Also available option --iterations, that option represent how much times philosopher will be eat.
# That option has default value: 100.

# For avoiding deadlocks implementation with default iterations count.
python philosophers_problem.py --implementation avoiding-deadlocks

# For avoiding deadlocks implementation with custom iterations count.
python philosophers_problem.py --implementation avoiding-deadlocks --iterations 10

# For preventing deadlocks implementation with default iterations count.
python philosophers_problem.py --implementation preventing-deadlocks

# For preventing deadlocks implementation with custom iterations count.
python philosophers_problem.py --implementation preventing-deadlocks --iterations 10
```