"""
Lesson 16: Program multiply two matrices.
"""
from argparse import ArgumentParser, Namespace
from concurrent.futures import ProcessPoolExecutor

from typing import Tuple, NoReturn, List


def get_args() -> Namespace:
    """
    Add argument parser to script and return namespace with parsed command line arguments.

    :return: Namespace with parsed command line arguments.
    :rtype: Namespace
    """
    parser = ArgumentParser()
    parser.add_argument('--input-file', type=str, required=True, help='Input file with two matrices')
    return parser.parse_args()


class Matrices(object):
    """
    Class represent two matrices, row and column indexes which should be multiplied.
    """

    matrix_a: List[List[float]]
    matrix_b: List[List[float]]
    row_index: int
    column_index: int

    def __init__(self,
                 row_index: int,
                 column_index: int,
                 matrix_a: List[List[float]],
                 matrix_b: List[List[float]]) -> NoReturn:
        self.row_index = row_index
        self.column_index = column_index
        self.matrix_a = matrix_a
        self.matrix_b = matrix_b

    def get_row(self) -> List[float]:
        """
        Method return specified row from matrix a.

        :return: Specific row from matrix.
        :rtype: List[float]
        """
        return self.matrix_a[self.row_index]

    def get_column(self) -> List[float]:
        """
        Method return specified column from matrix b.

        :return: Specific column from matrix.
        :rtype: List[float]
        """
        column = []
        for row in self.matrix_b:
            for column_index, column_value in enumerate(row):
                if column_index == self.column_index:
                    column.append(column_value)
        return column


def multiply_row_and_column(element_pointer: Matrices) -> Tuple[int, int, float]:
    """
    Multiply row and column.

    :param element_pointer: Element represent two matrices, row and column which shall be multiplied.
    :type element_pointer: Matrices
    :return: Sum of row and column multiplication.
    :rtype: Tuple[int, int, float]
    """
    row = element_pointer.get_row()
    column = element_pointer.get_column()
    multiplication_result = sum(row[i] * column[i] for i in range(len(row)))
    return element_pointer.row_index, element_pointer.column_index, multiplication_result


def read_matrix(input_file: str) -> Tuple[List[List[float]], List[List[float]]]:
    """
    Read two matrix from file and return them.

    :param input_file: File with input, contains two matrices.
    :type input_file: str
    :return: Two matrices from input file.
    :rtype: Tuple[List[List[float]], List[List[float]]]
    """
    with open(file=input_file, mode='r', encoding='UTF8') as file:
        lines = file.readlines()
        matrix_a = []
        matrix_b = []
        filled_matrix = matrix_a
        for line in lines:
            line = line.rstrip()
            if not line:
                filled_matrix = matrix_b
                continue
            filled_matrix.append([float(x) for x in line.split()])
        return matrix_a, matrix_b


def multiply_matrix(matrix_a: List[List[float]], matrix_b: List[List[float]]) -> List[List[float]]:
    """
    Method multiply two matrices and return new matrix.

    :param matrix_a: First matrix to multiply.
    :type matrix_a: List[List[float]]
    :param matrix_b: Second matrix to multiply.
    :type matrix_b: List[List[float]]
    :return: New matrix, result of multiplication of two matrices.
    :rtype: List[List[float]]
    """
    with ProcessPoolExecutor() as executor:
        matrix_a_rows, matrix_b_columns = len(matrix_a), len(matrix_b[0])
        multiplied_matrix = [[0 for _ in range(matrix_b_columns)] for _ in range(matrix_a_rows)]
        row_by_column = []
        for i in range(matrix_a_rows):
            for j in range(matrix_b_columns):
                row_by_column.append(Matrices(i, j, matrix_a, matrix_b))
        futures = executor.map(multiply_row_and_column, row_by_column)
        for row_index, column_index, value in futures:
            multiplied_matrix[row_index][column_index] = value
        return multiplied_matrix


def print_matrix(matrix: List[List[float]]) -> NoReturn:
    """
    Print matrix by rows and columns.

    :param matrix: Matrix to print.
    :type matrix: List[List[float]]
    """
    for row in matrix:
        for column in row:
            print(f'{column:5.2} ', end='')
        print()


def main(input_file: str) -> NoReturn:
    """
    Main method (entry point).

    :param input_file: File with two matrices.
    :type input_file: str
    """
    matrix_a, matrix_b = read_matrix(input_file)
    multiplied_matrix = multiply_matrix(matrix_a, matrix_b)
    print_matrix(multiplied_matrix)


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments shall be taken from command line and then
    execute main method.
    """
    args = get_args()
    main(args.input_file)
