"""
Lesson 6: Program illustrate producer - consumer problem (implemented on semaphore).
"""
import argparse
import threading
import time

from random import random
from typing import NoReturn


def get_args() -> argparse.Namespace:
    """
    Add argument parser to script and return namespace with parsed command line arguments.

    :return: Namespace with parsed command line arguments.
    :rtype: argparse.Namespace
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--buffer-size', type=int, default=10, help='Buffer size in queue.')
    return parser.parse_args()


class MyQueue:
    """
    Class represent queue for producer and consumer. Producer and Consumer compete for the resources, MyQueue class
    represent who has access to the resources.
    """
    __lock: threading.Semaphore
    __full: threading.Semaphore
    __empty: threading.Semaphore
    __buffer: list
    __count: int
    __write_position: int
    __read_position: int

    def __init__(self, buffer_size: int) -> NoReturn:
        """
        Init method for queue.

        :param buffer_size: Size of the buffer with resources.
        :type buffer_size: int
        """
        self.__lock = threading.Semaphore()
        self.__full = threading.Semaphore(buffer_size)
        self.__empty = threading.Semaphore(buffer_size)
        self.__buffer_size = buffer_size
        self.__buffer = [None] * self.__buffer_size
        self.__count = 0
        self.__write_position = 0
        self.__read_position = 0
        for _ in range(buffer_size):
            self.__empty.acquire()

    def enqueue(self, resource: any) -> NoReturn:
        """
        Method used by producer to produce resource.

        :param resource: Some representation of resource.
        :type resource: any
        """
        self.__full.acquire()
        with self.__lock:
            print(f'Adding {resource}')
            self.__buffer[self.__write_position] = resource
            self.__count += 1
            self.__write_position = (self.__write_position + 1) % self.__buffer_size
        self.__empty.release()

    def dequeue(self, thread_number: int) -> any:
        """
        Method used by consumer to consume resource.

        :param thread_number: Number of thread which represent id of consumer who win that round in competition for
        resources.
        :type thread_number: int
        :return: Resource element.
        :rtype: any
        """
        self.__empty.acquire()
        with self.__lock:
            element = self.__buffer[self.__read_position]
            print(f'Thread {thread_number} is removing {element}')
            self.__buffer[self.__read_position] = None
            self.__count -= 1
            self.__read_position = (self.__read_position + 1) % self.__buffer_size
        self.__full.release()
        return element


class Producer(threading.Thread):
    """
    Class represent a producer.
    """
    __queue: MyQueue
    __thread_number: int

    def __init__(self, queue: MyQueue, thread_number: int) -> NoReturn:
        """
        Init method for producer, take queue and unique producer thread identifier.

        :param queue: Queue shared between producer and consumer.
        :type queue: MyQueue
        :param thread_number: Unique producer thread identifier.
        :type thread_number: int
        """
        threading.Thread.__init__(self)
        self.__queue = queue
        self.__thread_number = thread_number

    def run(self) -> NoReturn:
        """
        Start competition for the resources.
        """
        for index in range(10):
            time.sleep(random())
            self.__queue.enqueue((self.__thread_number, index))


class Consumer(threading.Thread):
    """
    Class represent a consumer.
    """
    __queue: MyQueue
    __thread_number: int

    def __init__(self, queue: MyQueue, thread_number: int) -> NoReturn:
        """
        Init method for consumer, take queue and unique consumer thread identifier.

        :param queue: Queue shared between consumer and producer.
        :type queue: MyQueue
        :param thread_number: Unique consumer thread identifier.
        :type thread_number: int
        """
        threading.Thread.__init__(self)
        self.__queue = queue
        self.__thread_number = thread_number

    def run(self) -> NoReturn:
        """
        Start competition for the resources.
        """
        for _ in range(10):
            time.sleep(random())
            self.__queue.dequeue(self.__thread_number)


def main(buffer_size: int) -> NoReturn:
    """
    Main method (entry point).

    :param buffer_size: Size of the queue buffer.
    :type buffer_size: int
    """
    queue = MyQueue(buffer_size)
    threads = [Producer(queue, number) for number in range(10)]
    threads += [Consumer(queue, number) for number in range(10)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments shall be taken from command line, and then
    execute main method.
    """
    args = get_args()
    main(args.buffer_size)
