# Lesson 6: producer - consumer problem (implemented on semaphore)

**Usage:** 

```
# Using defualt buffer size. Default buffer size is 10.
python producer_consumer.py

# With specific buffer size.
python producer_consumer.py --buffer-size 15
```