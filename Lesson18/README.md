# Lesson 18: Client-Server application, uses TCP protocol to demonstrate how to implement messages transmitting

**Program consists from two parts:**
- Client
- Server

Both programs shall be run in the different docker containers.
Before you create and run containers, please prepare docker environment, for that you should read "Usage" section. \
Also please read usage section for client and server. These sections described in separate modules. \
**Important:** the server must always start before the client.

**Usage:** 

```
# Create new docker network.
docker network create -d bridge pwirnet
```