"""
Lesson 18: The server program, that listens on the specific port.
"""
from socket import AF_INET, SOCK_STREAM, socket
from pickle import dumps, loads
from threading import Thread, Lock
from typing import NoReturn

sum_result = 0


class ServerThread(Thread):
    """
    The server thread, handle requests for one specific connection (one socket).
    """

    __connection: socket = None
    __lock: Lock = None

    def __init__(self, connection: socket):
        Thread.__init__(self)
        self.__connection = connection
        self.__lock = Lock()

    def run(self):
        """
        Run new thread that handle one connection.
        """
        global sum_result
        number = 1
        while number > 0:
            data = self.__connection.recv(1024)
            number, value = loads(data)
            print(f'Received {number}, {value}')
            with self.__lock:
                sum_result += value
                answer = dumps((number + 1, sum_result))
                self.__connection.sendall(answer)
        self.__connection.close()


def main() -> NoReturn:
    """
    Main method (entry point).
    Run the server.
    """
    global sum_result
    with socket(AF_INET, SOCK_STREAM) as lsock:
        lsock.bind(('', 2000))
        print('Bound the listening socket to all interfaces on port 2000')
        lsock.listen()
        print('Started listening')
        while True:
            connection, address = lsock.accept()
            print(f'Accepted a connection on address {address}')
            new_thread = ServerThread(connection)
            new_thread.start()


if __name__ == '__main__':
    """
    If that module will be executed directly then executes main method.
    """
    main()
