# The server program

The server program listen on port 2000, if some client tries to connect, then server handles that connection.

**Usage:** 

```
# To create new image, try to excute the foolowing command.
# IMPORTANT: Execute command from that folder.
docker build -t servertcp .

# To create container and run new image use the following command.
docker run -it --name server --rm --network pwirnet servertcp

# To kill existing container, please execute next command.
docker kill server
```