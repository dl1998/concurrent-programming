"""
Lesson 18: The client program, that tries to send message to the server and wait on the response after.
"""
from argparse import Namespace, ArgumentParser
from socket import AF_INET, SOCK_STREAM, socket
from pickle import dumps, loads
from time import sleep
from random import random
from typing import NoReturn


def get_args() -> Namespace:
    """
    Add argument parser to script and return namespace with parsed command line arguments.

    :return: Namespace with parsed command line arguments.
    :rtype: Namespace
    """
    parser = ArgumentParser()
    parser.add_argument('hostname', type=str, help='Server hostname.')
    parser.add_argument('--port', type=int, required=False, default=2000, help='Server port.')
    return parser.parse_args()


def main(hostname: str, port: int) -> NoReturn:
    """
    Main method (entry point). Connects to the server, then send message to the server and wait on the response.

    :param hostname: Server hostname.
    :type hostname: str
    :param port: Server port.
    :type port: int
    """
    with socket(AF_INET, SOCK_STREAM) as server_socket:
        print(f'Connecting to {hostname}')
        server_socket.connect((hostname, port))
        for index in range(10, -1, -1):
            sleep(random())
            random_value = random()
            data = dumps((index, random_value))
            print(f'Sending ({index}, {random_value})')
            server_socket.sendall(data)
            answer = server_socket.recv(1024)
            server_counter, sum_result = loads(answer)
            print(f'Received ({server_counter}, {sum_result})')


if __name__ == '__main__':
    """
    If that module will be executed directly then first of all arguments should be taken from command line and then
    executes main method.
    """
    args = get_args()
    main(args.hostname, args.port)
