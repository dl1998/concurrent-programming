# The client program

The client program, takes server hostname and port (port is optional). Then tries to connect to the server and start 
sends messages to the server.

**Usage:** 

```
# To create new image, try to excute the foolowing command.
# IMPORTANT: Execute command from that folder.
docker build -t clienttcp .

# To create container and run new image use the following command.
docker run -it --name client --rm --network pwirnet clienttcp server

# To create container and run it using specific port use the following command. Default port is 2000.
# Below is an example how to run client and connect it to server with port 2001.
docker run -it --name client --rm --network pwirnet clienttcp server --port 2001

# To kill existing container, please execute next command.
docker kill client
```