"""
Lesson 15: Program creates 10 processes and token, then transmit token in chain.
"""
from multiprocessing import Process, Queue
from random import random
from time import sleep
from typing import NoReturn

SUCCESS_THRESHOLD: float = 0.005


class Token(object):
    """
    Class represent token.
    """
    kill: bool
    count: int

    def __init__(self, kill: bool, count: int):
        self.kill = kill
        self.count = count


class TokenProcess(Process):
    """
    Class represent one of the processes, which will transmit token.
    """

    __this_queue: Queue
    __next_queue: Queue

    def __init__(self, this_queue: Queue, next_queue: Queue) -> NoReturn:
        Process.__init__(self)
        self.__this_queue = this_queue
        self.__next_queue = next_queue

    @staticmethod
    def __token_info(token: Token) -> NoReturn:
        """
        Utility method, print information about token.

        :param token: Generated token.
        :type token: Token
        """
        print(f'Token received, kill: {token.kill}, count: {token.count}')

    @staticmethod
    def __generate_digit_and_check_status():
        """
        Generate random digit and check is it less then success threshold, if yes, then finish execution, else continue.
        Additionally method print information about generated digit and condition.
        """
        digit = random()
        if digit >= SUCCESS_THRESHOLD:
            status, result = 'is not met, continue execution', False
        else:
            status, result = 'is met, finish execution', True
        print(f'Condition: {digit:.3f} < {SUCCESS_THRESHOLD} {status}.')
        return result

    def run(self):
        """
        Get token from queue, check it, if that not dead token (kill value is False) generate new and send it to next
        process. Else change kill value on True, send it to next process and complete execution.
        """
        while True:
            token: Token = self.__this_queue.get()
            self.__token_info(token)
            sleep(random())
            if token.kill is False:
                if self.__generate_digit_and_check_status():
                    token.kill = True
                self.__next_queue.put(token)
            else:
                if token.count > 0:
                    token.count -= 1
                    self.__next_queue.put(token)
                break


def main():
    """
    Main method (entry point).
    """
    queues = [Queue() for _ in range(10)]
    queues[0].put(Token(False, 10))
    token_processes = []
    for index in range(10):
        token_processes.append(TokenProcess(queues[index], queues[(index + 1) % 10]))
    for process in token_processes:
        process.start()
    for process in token_processes:
        process.join()


if __name__ == '__main__':
    """
    If that module will be executed directly then execute main method.
    """
    main()
